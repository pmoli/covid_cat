import React, { useState, useEffect } from "react";

import { Container, Table } from "reactstrap";
import styled from "styled-components";

const IconoCentrado = styled.i`
  position: absolute;
  left: 0;
  right: 0;
  top: 200px;
  width: 100%;
`;

const Meteo = () => {
  const [llista, setLlista] = useState([]);
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const ciutat = "granada,es";
    const apiKey = "84dbcf8c3480649bce9d4bb58da44b4e";
    const funcio = "forecast";
    const apiUrl = `http://api.openweathermap.org/data/2.5/${funcio}?q=${ciutat}&APPID=${apiKey}&units=metric`;
    fetch(apiUrl)
      .then((response) => response.json())
      .then((data) => {
        setLlista(data.list);
        setLoading(false);
      })
      .catch((error) => setError(true));

    console.log("peticion enviada");

  }, []);




  if (error) {
    return <h3>Se ha producido un error...</h3>;
  }


  if (loading) {
    return <IconoCentrado className="fa fa-spinner fa-spin fa-2x fa-fw" />;
  }


  
  let fecha = new Date(llista[0].dt * 1000).toLocaleString();
  let fecha2 = new Date(llista[8].dt * 1000).toLocaleString();



  return (
    <Container>
      <Table striped>
        <thead>
          <tr>

            <th>{fecha}</th>
            <th>{fecha2}</th>
          </tr>

        </thead>
        <tbody>
          <tr>
            <td>
              {llista[0].main.temp + ' ºC'}
            </td>
            <td>
              {llista[1].main.temp + ' ºC'}
            </td>
          </tr>




        </tbody>
        <tfoot>

        </tfoot>

      </Table>

    </Container>
  );
};

export default Meteo;
